# Imagen base
FROM node:latest
# Directorio de la app en el contenedor
WORKDIR /app
#Copiado de archivos

ADD build/es6-unbundled /app/build/es6-unbundled
ADD server.js /app
ADD package.json /app

# Dependencias
RUN npm install
# Puerto que expongo
EXPOSE 3000
# Comando para ejecutar el servicio
CMD ["npm", "start"]
